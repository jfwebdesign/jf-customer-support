<?
/*
** Plugin Name: JF WebDesign Customer Support Plugin
** Plugin URI: http://www.jfwebdesign.com/
** Description: This plugin adds a contact card to the dashboard for easy access to customer support. It also displays the active theme on the dashboard.
** Version: 1.5.2
** Author: JF WebDesign
** Author URI: http://www.jfwebdesign.com
** 
*/

function customer_service_enqueue_style() {
	wp_enqueue_style( 'jf_css_cs_styles', plugin_dir_url( __FILE__ ) . 'styles.css', false ); 
}

add_action( 'admin_enqueue_scripts', 'customer_service_enqueue_style' );

/* add Contact Card widget */

function jf_add_dashboard_widgets() {
	
	wp_add_dashboard_widget( 'jf_dashboard_widget', 'Welcome!', 'jf_dashboard_widget_render' );
 	
 	//Globalize the metaboxes array, this holds all the widgets for wp-admin
 	global $wp_meta_boxes;
 	
 	// Get the regular dashboard widgets array 
 	// (which has our new widget already but at the end)
 	$normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core'];
 	
 	// Backup and delete our new dashboard widget from the end of the array
 	$example_widget_backup = array( 'jf_dashboard_widget' => $normal_dashboard['jf_dashboard_widget'] );
 	unset( $normal_dashboard['jf_dashboard_widget'] );
 
 	// Merge the two arrays together so our widget is at the beginning
 	$sorted_dashboard = array_merge( $example_widget_backup, $normal_dashboard );
 
 	// Save the sorted array back into the original metaboxes 
 
 	$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
}
add_action( 'wp_dashboard_setup', 'jf_add_dashboard_widgets' );
/* render the jf dashboard widget */
function jf_dashboard_widget_render() 
{
	echo "<a href='http://www.jfwebdesign.com' target='_blank'><img src='". plugin_dir_url( __FILE__ ). "images/JF-WP-dashboard.jpg' style='width: 100%' alt='JF WebDesign Dashboard Image' title='JF WebDesign Dashboard Image'></a>";
} 

/* add Active Theme Widget */

function jf_add_active_theme_widgets() {
	
	wp_add_dashboard_widget( 'jf_dashboard_theme_widget', 'Active Theme', 'jf_dashboard_theme_widget_render' );
 	
 	//Globalize the metaboxes array, this holds all the widgets for wp-admin
 	global $wp_meta_boxes;
 	
 	// Get the regular dashboard widgets array 
 	// (which has our new widget already but at the end)
 	$normal_dashboard = $wp_meta_boxes['dashboard']['normal']['core'];
 	
 	// Backup and delete our new dashboard widget from the end of the array
 	$example_widget_backup = array( 'jf_dashboard_theme_widget' => $normal_dashboard['jf_dashboard_theme_widget'] );
 	unset( $normal_dashboard['jf_dashboard_theme_widget'] );
 
 	// Merge the two arrays together so our widget is at the beginning
 	$sorted_dashboard = array_merge( $example_widget_backup, $normal_dashboard );
 
 	// Save the sorted array back into the original metaboxes 
 
 	$wp_meta_boxes['dashboard']['normal']['core'] = $sorted_dashboard;
}
add_action( 'wp_dashboard_setup', 'jf_add_active_theme_widgets' );

function jf_dashboard_theme_widget_render() 
	{
		$my_theme = wp_get_theme();
		echo '<div style="text-align: center;">';
		echo '<img src="'.esc_url($my_theme->get_screenshot()).'" style="width: 100%;"/>';
		echo '</div>';
		echo '<div style="text-align: center;">';
		echo esc_html( $my_theme->get( 'Name' ) );
		echo '</div>';
	} 
/* remove standard dashboard widgets */

function jf_remove_dashboard_widgets(){
    remove_meta_box('dashboard_right_now', 'dashboard', 'normal');   // Right Now
    remove_meta_box('dashboard_recent_comments', 'dashboard', 'normal'); // Recent Comments
    remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');  // Incoming Links
    remove_meta_box('dashboard_plugins', 'dashboard', 'normal');   // Plugins
    remove_meta_box('dashboard_quick_press', 'dashboard', 'side');  // Quick Press
    remove_meta_box('dashboard_recent_drafts', 'dashboard', 'side');  // Recent Drafts
    remove_meta_box('dashboard_primary', 'dashboard', 'side');   // WordPress blog
    remove_meta_box('dashboard_secondary', 'dashboard', 'side');   // Other WordPress News
    remove_meta_box('dashboard_activity', 'dashboard', 'normal');   // Activity
    remove_meta_box('wpe_dify_news_feed', 'dashboard', 'normal');   // WPE news feed
	//use 'dashboard-network' as the second parameter to remove widgets from a network dashboard.
}
add_action('wp_dashboard_setup', 'jf_remove_dashboard_widgets');

// Hook for adding admin menus
add_action('admin_menu', 'add_license_menu');
function add_license_menu() {
    // Main Page:
    add_menu_page(__('JF Web CS','cs_1'), __('JF Web CS','cs_1'), 'manage_options', 'cs-top-level-handle', 'cs_toplevel_page' );
	// Licenses
	//add_submenu_page('cs-top-level-handle', __('Manage Licenses','cs_2'), __('Manage Licenses','cs_2'), 'manage_options', 'manage_licenses', 'customer_support_plugin_license_management_page');
}

// Manage Licenses
function cs_toplevel_page() {
    echo "<h2>" . __( 'Customer Service Information', 'cs_1' ) . "</h2>";
	echo "<a href='http://www.jfwebdesign.com' target='_blank'><img src='". plugin_dir_url( __FILE__ ). "images/JF-WP-dashboard.jpg' alt='JF WebDesign Dashboard Image' title='JF WebDesign Dashboard Image' class='banner'></a>";

	if ( is_plugin_active( 'wppusher/wppusher.php' ) ) {
echo "
<h3>Available Plugins to Add Through WP Pusher</h3>
<ul>
	<li>Customer Support Plugin: jfwebdesign/jf-customer-support</li>
	<li>Custom Button Widget and Shortcode: jfwebdesign/jf-button-widget</li>
	<li>Custom Endorsement Page: jfwebdesign/jf-endorsements</li>
	<li>Force Use Image for Social Media: jfwebdesign/jf-social-media-image</li>
	<li>Disable Pingback: jfwebdesign/jf-webdesign-disable-pingback</li>
</ul>
";

	}
}

	
// Functions
function check_for_pusher_plugin() {
	//if ( is_plugin_active( 'wppusher/wppusher.php' ) ) {require_once( plugin_dir_path( __FILE__ ) . 'functions.php' );}
}
//add_action( 'admin_init', 'check_for_pusher_plugin' );
?>