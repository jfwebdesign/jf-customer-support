<?
/// This is the secret key for API authentication. You configured it in the settings menu of the license manager plugin.
define('JF_KEY', '59cc0899521462.16551783'); //Rename this constant name so it is specific to your plugin or theme.
// This is the URL where API query request will be sent to. This should be the URL of the site where you have installed the main license manager plugin. Get this value from the integration help page.
define('JF_URL', 'https://www.jfwebdesign.com'); //Rename this constant name so it is specific to your plugin or theme.
// This is a value that will be recorded in the license manager data so you can identify licenses for this item/product.
define('JF_ITEM', 'Customer Support Plugin'); //Rename this constant name so it is specific to your plugin or theme.
?>