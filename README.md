=== JF WebDesign Customer Support Plugin ===
Tags: page, category, category in page, tag in page, page archive, pages  
Requires at least: 3.3
Tested up to: 4.8
Stable tag: 1.0

JF WebDesign Customer Support Plugin

== Description ==
This plugin adds a contact card to the dashboard for easy access to customer support.

== Changelog ==


= Version 1.0.1 =
* Initial Plugin Release


== Installation ==

This section describes how to install the plugin and get it working.

1. Unzip archive and upload the entire folder to the /wp-content/plugins/ directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. It will do the rest.


== Frequently Asked Questions ==

None.